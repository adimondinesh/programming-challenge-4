﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CatchPokemon : MonoBehaviour
{
    // The following Serializable classes have been created to store the required data recived from a JSON file

    [System.Serializable]
    public class Sprites
    {
        public string front_default;    // Image of Pokemon
    }

    [System.Serializable]
    public class Species
    {
        public string name;             // Name of Pokemon
    }

    [System.Serializable]
    public class PokemonData
    {
        public Sprites sprites;         // Sprites for a particular Pokemon
        public Species species;         // Species for a particular Pokemon
    }

    public Text pokemonID;              // Input field to enter Pokemon ID(or Name)
    public Image pokemonImg;            // UI Image of Pokemon obtained from PokemonID
    public Text pokemonName;            // UI Text to display Name of Pokemon obtained from PokemonID
    private float updatePokemonTime = 5.0f;     // A wait timer to update Pokemon fetched from PokemonID

    private Texture2D tex;              // Texture which will be obtained from byte array
    private float currentTime = 0.0f;   // Variable to store current time

    void Start()
    {
        CanvasManager.Instance.infoText.text = "Welcome " + PlayFabLogin.Instance.username;     // Greet the Player
        currentTime = updatePokemonTime;                                                        // Set the Wait Timer
        tex = new Texture2D(96, 96);                                                            // Set the Dimensions of the texture
    }

    public void Update()
    {
        currentTime -= Time.deltaTime;

        if(currentTime <= 0)
        {
            PokemonData response = getPokemonData();        // Fetch pokemon data JSON file with help of a URL

            if (response != null)
            {
                // Store the name of Pokemon recived from JSON and fetch the byte array for the sprite of Pokemon
                if (!string.IsNullOrEmpty(response.species.name) && !string.IsNullOrEmpty(response.sprites.front_default))
                {
                    pokemonName.text = response.species.name;
                    StartCoroutine(SetByteArray(response.sprites.front_default));
                }
                else
                {
                    CanvasManager.Instance.infoText.text = "Enter Pokemon ID or Name to Load pokemon and then Hit 'Catch' to catch the pokemon";
                }
            }

            currentTime = updatePokemonTime;
        }
    }

    // Store name of Caught Pokemon and Location of player to PlayFab
    public void OnCatchPokemon()
    {
        // Get Location 
        LocationServicesController.Instance.GetLocation();  
        string location = LocationServicesController.Instance.location;

        // Upload Data to Playfab
        PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest()
        {
            Body = new Dictionary<string, object>() {
            { "Pokemon", pokemonName.text },
            { "Location", location }
        },
            EventName = "Pokemon_Caught"
        },
     result => onCaughtPokemon(),
     error => CanvasManager.Instance.infoText.text = error.GenerateErrorReport());;
    }

    // Fetch a sprite from a byte array
    IEnumerator SetByteArray(string url)
    {
        // Fetch byte array for the image from url
        UnityWebRequest request = UnityWebRequest.Get(url); 
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            CanvasManager.Instance.infoText.text = request.error;
        }
        else
        {
            // Once image is fetched, load it into a texture and convert it to sprite
            byte[] results = request.downloadHandler.data;
            tex.LoadImage(results);
            pokemonImg.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0,0,96,96), new Vector2(0,0),100.0f);
        }
    }
    
    // Fetch the JSON file containing Pokemon Data 
    private PokemonData getPokemonData()
    {
        // Format the url to fetch the pokemon data of the pokemon ID provided by player
        string url = string.Format("https://pokeapi.co/api/v2/pokemon/{0}", pokemonID.text);

        // Fetch the JSON file from above url
        try
        {
            HttpWebRequest request = WebRequest.CreateHttp(url);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());

            string jsonResponse = reader.ReadToEnd();

            reader.Close();
            reader.Dispose();

            return JsonUtility.FromJson<PokemonData>(jsonResponse);
        }
        catch (Exception ex)
        {
            CanvasManager.Instance.infoText.text = ex.Message + "." + ex.StackTrace;
            return null;
        }
    }

    // Once Pokemon is caught has been uploaded to PLayfab inform the player and update leaderboard
    private void onCaughtPokemon()
    {
        CanvasManager.Instance.infoText.text = string.Format("{0} was caught successfully", pokemonName.text);
        Leaderboard.Instance.onPostTotalPokemon();
    }
}
