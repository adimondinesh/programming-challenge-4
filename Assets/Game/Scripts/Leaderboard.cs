﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : Singleton<Leaderboard>
{
    public int totalPokemon = 0;            // A variable to store total number of pokemon caught


    // Increments the total number of pokemon caught and uploads it to PlayFab
    public void onPostTotalPokemon()
    {
        totalPokemon++;

        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate { StatisticName = "total_pokemon", Value = totalPokemon }
                }
            },
            new System.Action<UpdatePlayerStatisticsResult>(UpdatePlayerStatisticsResponse),
            new System.Action<PlayFabError>(PostLeaderboardErrorCallback)
        );
    }

    // Print the updated number of Pokemon Caught from PlayFab and Display it in the game
    public void UpdatePlayerStatisticsResponse(UpdatePlayerStatisticsResult result)
    {
        CanvasManager.Instance.infoText.text += string.Format(". You have caught a total of {0} pokemon uptill now ", totalPokemon);
    }

    // Fetch the total number of pokemon caught from PLayFab
    public void onGetTotalPokemon()
    {
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest
            {
                StatisticNames = new List<string>
                {
                    "total_pokemon"
                }
            },
            new System.Action<GetPlayerStatisticsResult>(PlayerStatisticResultCallback),
            new System.Action<PlayFabError>(GetLeaderboardErrorCallback)
        );
    }

    // Once the Total number of Pokemon is fetched from PlayFab set the local variable for total pokemon
    public void PlayerStatisticResultCallback(GetPlayerStatisticsResult result)
    {
        foreach (var statResult in result.Statistics)
        {
            if (statResult.StatisticName == "total_pokemon")
            {
                totalPokemon = statResult.Value;
                break;
            }
        }
    }

    // Print Error messages 
    public void GetLeaderboardErrorCallback(PlayFabError error)
    {
        CanvasManager.Instance.infoText.text = "Could not fetch Total Pokemon. Looks this is your first time catching one";
    }

    public void PostLeaderboardErrorCallback(PlayFabError error)
    {
        CanvasManager.Instance.infoText.text = "Could not Update Total Pokemon caught";
    }
}
