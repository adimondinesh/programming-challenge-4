﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayFabLogin : Singleton<PlayFabLogin>
{
    public Text UsernameText;           // Input Field for Username
    public string playerGUID;           // Player ID

    public string username;             // String to store username
    private CanvasManager canvas;       // Object to use CanvasManager which is a Singleton
    private bool createPlayer = false;  // Bool to check whether a new player should be created

    private void Awake()
    {
        // If no player ID is stored then create a player ID
        playerGUID = PlayerPrefs.GetString("PlayFabPlayerId");
        if(string.IsNullOrEmpty(playerGUID))
        {
            playerGUID = System.Guid.NewGuid().ToString();
            PlayerPrefs.SetString("PlayFabPlayerId", playerGUID);
            createPlayer = true;
        }

        canvas = CanvasManager.Instance;
        canvas.SetUsernamePanel();
        CanvasManager.Instance.infoText.text = "Enter a Username to Begin";
    }

    void Start()
    {
        // If Title ID for PlayFab is not assigned then assign it here
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = "C25B3";
        }

        // Login to PlayFab with playerID
        var request = new LoginWithCustomIDRequest { CustomId = playerGUID, CreateAccount = createPlayer };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);

        // If username is already stored then switch to Catch Pokemon Panel
        if (LoadUserPrefs())
        {
            canvas.SetCatchPokemonPanel();
        }
    }

    // Update username to PlayFab if not stored after clicking the Enter button
    public void OnClickEnter()
    {
        var req = new UpdateUserTitleDisplayNameRequest { DisplayName = UsernameText.text };
        PlayFabClientAPI.UpdateUserTitleDisplayName(req, OnUpdateUsernameSuccess, OnUpdateUsernameFailure);
    }

    // If a new username has been entered then locally store it 
    private void SaveUserPrefs()
    {
        PlayerPrefs.SetString("Username", UsernameText.text);
        username = UsernameText.text;
        canvas.SetCatchPokemonPanel();
    }

    // Check if a username is already stored
    private bool LoadUserPrefs()
    {
        if (PlayerPrefs.HasKey("Username"))
        {
            username = PlayerPrefs.GetString("Username");
            return true;
        }
        return false;
    }

    // Once the player has logged in fetch the total pokemon caught 
    private void OnLoginSuccess(LoginResult _result)
    {
        Leaderboard.Instance.onGetTotalPokemon();
    }

    private void OnLoginFailure(PlayFabError _error)
    {
        canvas.infoText.text = "Login Failure";
    }

    // If username is updated on Playfab then store it locally as well
    private void OnUpdateUsernameSuccess(UpdateUserTitleDisplayNameResult _result)
    {
        canvas.infoText.text = "UserName Updated Successfully";
        SaveUserPrefs();
    }

    private void OnUpdateUsernameFailure(PlayFabError _error)
    {
        canvas.infoText.text = "_error.GenerateErrorReport()";
    }

}
