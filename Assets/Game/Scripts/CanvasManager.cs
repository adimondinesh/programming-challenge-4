﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : Singleton<CanvasManager>
{
    public GameObject UsernamePanel;           // The Panel for entering Username 
    public GameObject CatchPokemonPanel;       // The Panel for Catching Pokemon
    public Text infoText;                      // A UI text used to display information to the player

    // Set the Username Panel
    public void SetUsernamePanel()
    {
        UsernamePanel.SetActive(true);
        CatchPokemonPanel.SetActive(false);
    }

    // Set the Catch Pokemon Panel
    public void SetCatchPokemonPanel()
    {
        UsernamePanel.SetActive(false);
        CatchPokemonPanel.SetActive(true);
    }
}
