﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationServicesController : Singleton<LocationServicesController>
{
    // Enum states which show status of Location Services
    public enum LocationServicesState
    {
        Waiting,
        Searching,
        Ready,
        Failed
    }

    public LocationServicesState state = LocationServicesState.Waiting;

    public float latitude;      // Variable to store latitude
    public float longitude;     // Variable to store longitude

    public string location;     // String which will store both longitude and latitude to be uploaded to PLayfab

    // Fetches the current location of the device using location services
    public void GetLocation()
    {
        if (state != LocationServicesState.Searching)
        {
            state = LocationServicesState.Searching;
            StartCoroutine(LocationServiceUpdate());
        }
    }

    IEnumerator LocationServiceUpdate()
    {
        Input.location.Start();

        int waitTime = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && waitTime > 0) // Wait for some time till Location data is recieved
        {
            yield return new WaitForSeconds(1);
            waitTime--;
        }

        if (waitTime <= 0)      // If location data not recived 
        {
            location = "Not Available, Service Timed Out";
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped)    // If Location Services failed
        {
            location = "Not Available, Failed or Stopped";
            state = LocationServicesState.Failed;
            yield break;
        }

        latitude = Input.location.lastData.latitude;    // Set Latitude
        longitude = Input.location.lastData.longitude;  // Set Longitude

        location = string.Format("Lat:{0},Lon:{1}", latitude, longitude);   // Set Location containg both longitude and latitude

        Input.location.Stop();

        state = LocationServicesState.Ready;

        Debug.Log(location);
    }
}
